/*
    Copyright (C) 2016 Apple Inc. All Rights Reserved.
    See LICENSE.txt for this sample’s licensing information
    
    Abstract:
    The primary view controller. The speach-to-text engine is managed an configured here.
*/
/*
public protocol SFSpeechRecognitionTaskDelegate : NSObjectProtocol {
    
    
    // Called when the task first detects speech in the source audio
    optional public func speechRecognitionDidDetectSpeech(_ task: SFSpeechRecognitionTask)
    
    
    // Called for all recognitions, including non-final hypothesis
    optional public func speechRecognitionTask(_ task: SFSpeechRecognitionTask, didHypothesizeTranscription transcription: SFTranscription)
    
    
    // Called only for final recognitions of utterances. No more about the utterance will be reported
    optional public func speechRecognitionTask(_ task: SFSpeechRecognitionTask, didFinishRecognition recognitionResult: SFSpeechRecognitionResult)
    
    
    // Called when the task is no longer accepting new audio but may be finishing final processing
    optional public func speechRecognitionTaskFinishedReadingAudio(_ task: SFSpeechRecognitionTask)
    
    
    // Called when the task has been cancelled, either by client app, the user, or the system
    optional public func speechRecognitionTaskWasCancelled(_ task: SFSpeechRecognitionTask)
    
    
    // Called when recognition of all requested utterances is finished.
    // If successfully is false, the error property of the task will contain error information
    optional public func speechRecognitionTask(_ task: SFSpeechRecognitionTask, didFinishSuccessfully successfully: Bool)
}
*/
import UIKit
import Speech
import CallKit
import Accelerate

public class ViewController: UIViewController, SFSpeechRecognizerDelegate {
    
    
    var dellAssistance:Articulate?
    
    // MARK: Properties
    @IBOutlet var textView : UITextView!
    
    @IBOutlet var recordButton : UIButton!
    
    // MARK: UIViewController
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        //print(Intent.Meeting.Create.init(rawValue: "create_meeting_exit"))
        self.recordButton.isEnabled = false
        
        let user = DAUser()
        user.email = "vasudha.jha@emc.com"
        user.name  = "Vasudha Jha"
        user.userid = 1115466
        user.timezone = "+05:30"
        user.isFromDell = "no"
        
        dellAssistance = Articulate(user: user, baseURl: "https://ssgosgetst.emc.com/TEST/dellrestapi/")
        
        dellAssistance?.headers = ["Content-Type": "application/json",
                                   "Accept": "application/json",
                                   "Authorization":"Bearer 64beb09d-4033-4439-899e-921c3fff388f",
                                   "accessToken": "fR3nT6*2mVrq1@pOzLTur7@pxOzbLeiTuvr$7g5RVr1"
                                  ]
        dellAssistance?.delegate = self
        dellAssistance?.setup()
        dellAssistance?.authorize()
        dellAssistance?.stateCompletion = { (state) in
            
            switch state {
            case .suspended, .completed, .canceling:
                self.recordButton.isEnabled = true
                self.recordButton.setTitle("Start Listening", for: .normal)
                
            default:
                break
            }
        }
    }
    
    // MARK: Interface Builder actions
    @IBAction func recordButtonTapped() {
        
        dellAssistance?.startListening()
    }
    
//    private func speak() {
//        message.text = "welcome to VDS Text to speech module. Hope you enjoy"
//
//        let vds = VDSTextToSpeech.shared
//        vds.speechSynthesizerDelegate = self
//
//        vds.text = message.text ?? "no text found."
//
//        vds.speak()
//    }
}

extension ViewController:ArticulationDelegate {
    
    
    func dellAssistant(_ state:ArticulationState, didAuthorize status: SFSpeechRecognizerAuthorizationStatus, error:Error?) {
        
        /*
         The callback may not be called on the main thread. Add an
         operation to the main queue to update the record button's state.
         */        
        OperationQueue.main.addOperation {
            
            switch status {
            case .authorized:
                self.recordButton.isEnabled = true
                
                
            case .denied:
                self.recordButton.isEnabled = false
                self.recordButton.setTitle("User denied access to speech recognition", for: .disabled)
                
            case .restricted:
                self.recordButton.isEnabled = false
                self.recordButton.setTitle("Speech recognition restricted on this device", for: .disabled)
                
            case .notDetermined:
                self.recordButton.isEnabled = false
                self.recordButton.setTitle("Speech recognition not yet authorized", for: .disabled)
            }
        }
    }
    
    func dellAssistant(_ state: ArticulationState, didReceive utterance: String?) {
        
        self.textView.text = utterance
        self.recordButton.setTitle("Go ahead, I'm listening", for: .normal)

    }
    
    func dellAssistant(_ state: ArticulationState, didFinish utterance: String?) {
        
        self.textView.text = utterance
        self.recordButton.setTitle("Getting your information", for: .normal)
    }
    
    func dellAssistant(_ state: ArticulationState, willRender data: Data?) {
        
        self.recordButton.setTitle("Getting your information", for: .normal)

    }
    
    func dellAssistant(_ state: ArticulationState, didFail error: Error?) {
        
    }
}

extension ViewController: VDSSpeechSynthesizerDelegate {
    //MARK:- VDSSpeechSynthesizerDelegate
    func speechSynthesizerProgress(_ progress: Float, attributedText: NSMutableAttributedString) {
       // message.attributedText = attributedText
        //progressView.progress = progress
    }
    
    func speechSynthesizerDidStart() {
        print("speechSynthesizerDidStart")
    }
    
    func speechSynthesizerDidFinish() {
        print("speechSynthesizerDidFinish")
    }
}


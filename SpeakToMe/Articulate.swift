//
//  Articulate.swift
//  SpeakToMe
//
//  Created by Bogaraj, Jegadesh on 28/03/19.
//  Copyright © 2019 Henry Mason. All rights reserved.
//

import Foundation
import Speech
import CallKit
import Accelerate

enum ArticulationState {
    
    case start
    case idle
    case listen
    case process
    case speak
    case stop
}

enum VoiceSDKState {
    
    case start
    case stop
}


final class Articulate: NSObject {
    
    public enum VoiceAssistantState : Int {
        
        case notDetermined
        
        case ready
        
        case running
        
        case suspended
        
        case canceling
    }
    
    //ON state change implement didSet and send a callback to viewcontroller to dimiss the Assitance
    var state:VoiceAssistantState = .notDetermined {
        
        
        didSet {
            
            self.stateCompletion?(state)
        }
    }
    
    internal var transcribe: Transcribe?
    var delegate:ArticulationDelegate?
    
    private let speechSynthesizer = Dictation.shared
    private var user:DAUser?
    private var baseURl:String = "http://52.221.164.89"

    var articulationState: ArticulationState = .stop
    var voiceSDKState: VoiceSDKState = .stop
    var authStatus: SFSpeechRecognizerAuthorizationStatus = .notDetermined
    private var timer: Timer?

    var headers: [String : String] = ["Content-Type": "application/json;charset=utf-8",
                                      "Accept": "application/json"]
    
    public var assistantTimeoutInterval: TimeInterval = 30 {
        didSet {
            //startTimerCounter()
        }
    }
    
    var stateCompletion: ((VoiceAssistantState) -> Void)?

    
    override init() {
        super.init()
        
    }
    
    convenience init(user:DAUser, baseURl:String) {
        
        self.init()
        self.user = user
        self.baseURl = baseURl
    }
    
    public func setup() {
        
        self.speechSynthesizer.speechSynthesizerDelegate = self
        
        setupSpeechRecognizer()
    }
    
    
    private func setupSpeechRecognizer() {
        
        self.transcribe = Transcribe()
        self.transcribe?.delegate = self

        let locales = SFSpeechRecognizer.supportedLocales().filter({ $0.languageCode == Locale.current.languageCode })
        
        let locale = locales.first
        
        self.transcribe?.speechRecognizer = SFSpeechRecognizer(locale: locale ?? Locale(identifier: "en-US"))!
        debugPrint(#function,"speechRecognizer?.locale",self.transcribe?.speechRecognizer!.locale.languageCode ?? "")
    }
    
    private func startTimerCounter() {
        timer?.invalidate()
        timer = nil
        
        timer = Timer.scheduledTimer(timeInterval:assistantTimeoutInterval, target: self, selector: #selector(stopTimerCounter), userInfo: nil, repeats: false)
    }
    
    @objc private func stopTimerCounter() {
        
        self.state = .suspended
        
        timer?.invalidate()
        timer = nil
    }
    
    func runVoiceAssistant() -> Void {
        
        if self.state == .suspended {
            self.state = .running
        }
    }
    
    func authorize() -> Void {
        
        SFSpeechRecognizer.requestAuthorization { authStatus in
            /*
             The callback may not be called on the main thread. Add an
             operation to the main queue to update the record button's state.
             */
            
            switch authStatus {
            case .authorized:
                
                guard let user = self.user,
                    let email = user.email,
                    let userid = user.userid,
                    let name = user.name,
                    let timezone = user.timezone
                    else {
                        
                        let error = NSError(domain: "invalid", code: 6677, userInfo: ["error_message" : "user not found"])
                        self.delegate?.dellAssistant(self.articulationState, didFail: error)
                        return
                }
                
                let string = "\(self.baseURl)"+ENDPOINTS.SAVE_USER
                
                let parameters = ["dellUserId":"\(userid)","userTimeZone":timezone,"pushNotificationKey":"\(userid)", "fullName":name,"isFromDell":"no","userName":email]
                
                let service = DAServiceContext(urlString: string, allHTTPHeaderFields:self.headers)
                service.start(parameters) { (data, error) in
                    
                    var _error = error
                    
                    if _error != nil {
                        
                        self.authStatus = .restricted
                    }
                    else {
                        
                        do {
                            
                            guard let data = data
                                else {
                                    return
                            }
                            
                            let response   = try JSONDecoder().decode(DAUserResponse.self, from: data)
                            let status =  response.status.code
                            
                            if status == 200, let recepientid = response.user?.recepientid
                            {
                                self.state = .ready
                                
                                self.authStatus = .authorized
                                self.user?.recepientid = recepientid
                            }
                            else {
                                
                                self.authStatus = .restricted
                                
                                _error =  NSError(domain:"", code:status, userInfo:["error_message":response.status.message])
                            }
                            
                            //let jsonObject = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                            //utterance = response.first?.response.speech?.joined(separator: ",")
                        }
                        catch let error {
                            
                            debugPrint(error.localizedDescription)
                        }
                    }
                    
                    self.delegate?.dellAssistant(self.articulationState, didAuthorize: authStatus, error:_error)
                    
                }
                
            default:
                self.authStatus = authStatus
                self.delegate?.dellAssistant(self.articulationState, didAuthorize: authStatus, error:nil)
                
            }
        }
    }
    
    func startListening() {
        
        switch authStatus {
        case .authorized:
            
            if self.transcribe == nil {
                setupSpeechRecognizer()
            }
            
            self.runVoiceAssistant()
            self.transcribe?.startRecording()
            startTimerCounter()
            
        default:
            
            self.delegate?.dellAssistant(articulationState, didAuthorize: authStatus, error:nil)
        }
    }
    
    func stopListening()  {
        
        self.articulationState = .stop
        transcribe?.stopRecording()
        transcribe = nil
    }
}


extension Articulate:TranscriptionDelegate {
    
    func transcription(_ speechRecognizer: SFSpeechRecognizer?, didStart utterance: String?) {
        
        self.articulationState = .start
        print("started...")
    }
    
    func transcription(_ speechRecognizer: SFSpeechRecognizer?, didReceive utterance: String?) {
        
        self.articulationState = .listen
        
        self.delegate?.dellAssistant(articulationState, didReceive: utterance)
        print("listening...")
    }
    
    func transcription(_ speechRecognizer: SFSpeechRecognizer?, didFinish utterance: String?) {
        
        self.delegate?.dellAssistant(articulationState, didFinish: utterance)
        
        var q = utterance
        
        switch state {
        case .ready, .suspended:
            
            guard let utterance = utterance?.lowercased(),
                utterance.contains(HOTWORD)
                else {
                q = nil
                break
            }
            
            q = state == .ready ? "DELL_CHAIR_GLASS_TABLE" : "hi"

        case .canceling:
            
            q = nil
            
            break
        default:
            break
        }
        
        
        guard let user = self.user,
            let recepientid = user.recepientid,
            let sentence = q
            else {
                
                self.startListening()
                return
                
        }
        
        stopTimerCounter()
       
        self.articulationState = .process

        self.state = .running

        let string = "\(self.baseURl)"+ENDPOINTS.CONVERSATIONS.replacingOccurrences(of: "{sender}", with: "\(recepientid)")

        print("query:\(sentence)")
        
        let service = DAServiceContext(urlString: string, allHTTPHeaderFields:headers)
        service.delegate = self
        service.start(["query":sentence]) { (data, error) in
            
            var _error = error
            
            if _error != nil {
                
                self.authStatus = .restricted
            }
            else {
                
                do {
                    
                    guard let data = data
                        else {
                            return
                    }
                
                    
                    let JSON = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    print(JSON)
                    
                    var doc:DAElement?
                    
                    if let attachments = JSON as? [[AnyHashable:Any]],
                    let attachment = attachments.first,
                        attachment[RESPONSE.ATTACHMENT] != nil {
                        
                        let response   = try JSONDecoder().decode(DAResponse.self, from: data)
                        doc = response.first?.response
                    }
                    else if let forms = JSON as? [[AnyHashable:Any]],
                        let form = forms.first,
                        form[RESPONSE.BUTTONS] != nil
                    {
                        let response   = try JSONDecoder().decode(DAFormResponse.self, from: data)
                        doc = response.first?.response
                    }
                
                    
                    let utterance = doc?.speech?.joined(separator: ",") ?? FALLBACK.SPEECH_RESPONSE_NULL

                    /*
                    guard let utterance = text
                        else {
                            self.startListening()
                            return
                    }*/
                    
                    if let intent = doc?.intent {
                        
                        switch intent {
                        case "thank", "bye", "mood_unhappy", "mood_deny":
                            self.state = .suspended
                        default:
                            break
                        }
                        
                    }
                    
                    self.service(nil, willSpeakRangeOfSpeechString: utterance)
                    
                    self.delegate?.dellAssistant(self.articulationState, willRender: data)
                }
                catch let error {
                    
                    debugPrint(error.localizedDescription)
                    
                    self.service(nil, willSpeakRangeOfSpeechString: FALLBACK.INVALID_RESPONSE_TYPE)
                }
            }
        }
    }
    
    func transcription(_ speechRecognizer: SFSpeechRecognizer?, didCancel utterance: String?) {
        
        print("stopped...")
        
        switch articulationState {
        case .start, .listen, .stop:
            
            if let speechRecognizer = self.transcribe,
            speechRecognizer.isRunning {
                startListening()
            }
        default:
            break
        }
        
    }
    
    func transcription(_ speechRecognizer: SFSpeechRecognizer?, didError error: Error?) {
        
        print("error: \(error?.code)...")
        self.articulationState = .stop
    }
    
    func transcription(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        
    }
}

extension Articulate:SpeechSynthesizerDelegate {
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didStart utterance: AVSpeechUtterance) {
        
        stopListening()
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        
        startListening()
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didPause utterance: AVSpeechUtterance) {
        
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didContinue utterance: AVSpeechUtterance) {
        
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didCancel utterance: AVSpeechUtterance) {
        
        startListening()
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance) {
        
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, progress: Float, attributedText: NSMutableAttributedString) {
        
    }
    
    
}

extension Articulate:DAServiceContextDelegate {
    
    func service(_ request: URLRequest?, willSpeakRangeOfSpeechString utterance: String?) {
        
        self.articulationState = .speak
        self.speechSynthesizer.text = utterance ?? ""
        self.speechSynthesizer.speak()
        
    }
    
    func service(_ request: URLRequest?, didAuthorizationFail error: Error?) {
        
    }
    
    
}

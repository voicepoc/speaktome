//
//  ServiceContext.swift
//  SpeakToMe
//
//  Created by Bogaraj, Jegadesh on 02/04/19.
//  Copyright © 2019 Henry Mason. All rights reserved.
//

import Foundation


class DAServiceContext {
    
    private var urlString:String?
    private var headers = ["Content-Type": "application/json;charset=utf-8",
    "Accept": "application/json"]

    private let queue = OperationQueue.init()
    var delegate:DAServiceContextDelegate?

    init(urlString:String, allHTTPHeaderFields headers:[String:String]) {
        
        self.urlString = urlString
        self.headers = headers
    }
    
    func start(_ parameters: [String:String], completion: ((Data?, Error?) -> Void)?) {
        
        guard let urlString = urlString
            else {
            return
        }
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        
        for(key, value) in self.headers {
            request.addValue(value, forHTTPHeaderField: key)
        }
        
        do {
            let data = try JSONSerialization.data(withJSONObject: parameters)
            request.httpBody = data
        }
        catch let error {
            print("\(error.localizedDescription)")
        }
        
        
        let operation = DANetworkOperation(request) { (data, error) in
            
            if error != nil
            {
                guard let _error = error
                    else {
                        return
                }
                
                switch _error.code {
                    
                case NSURLErrorCannotFindHost, NSURLErrorCannotConnectToHost:
                    //"server is unavailable"
                    //utterance = ""
                    break
                case NSURLErrorNetworkConnectionLost, -1009:
                    //"Netowrk Error, Please Check your mobile network settings and try again"
                    break
                case 400, 401:
                    break
                case 500:
                    break
                default:
                    break
                }
            }
            
            //self.delegate?.service(request, willSpeakRangeOfSpeechString: utterance)

            completion?(data, error)
        }
        
        queue.addOperations([operation], waitUntilFinished: true)
        
    }

    func cancelAll() -> Void {
        
        queue.cancelAllOperations()
    }
    
}



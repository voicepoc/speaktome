//
//  SpeechSynthesizerConvertible.swift
//  SpeakToMe
//
//  Created by Bogaraj, Jegadesh on 26/03/19.
//  Copyright © 2019 Henry Mason. All rights reserved.
//

import Foundation
import Speech


public enum Intent:String {

    case greet = "greet"
    case bye = "bye"
    case thank = "thank"
    case launchApp = "launch_app"
    
    case moodAffirm = "mood_affirm"
    case moodDeny = "mood_deny"
    case moodGreat = "mood_great"
    case moodUnhappy = "mood_unhappy"
    case scopeOfWork = "scope_of_work"
    case outOfScope = "out_of_scope"
    case notesAffirm = "notes_affirm"
    case notesDeny = "notes_deny"
    
    
    enum Meeting:String {
        
        case repeatSchedule = "repeat_schedule"
        case cancelRepeatSchedule = "cancel_repeat_schedule"
        case organizer = "meeting_details_organizer"
        case start = "meeting_details_start"
        case end = "meeting_details_end"
        case duration = "meeting_details_duration"
        case attendees = "meeting_details_attendees"
        case location = "meeting_details_location"
        case join = "join_the_meeting"
        case discard = "discard_the_meeting"
        case find = "find_meeting"
        case reschedule = "reschedule_meeting"
        case schedules_query = "schedules_query"
        case scheduleDetails = "schedule_details"
        case scheduleCancel = "schedule_cancel"
        case scheduleCancelAsk = "schedule_cancel_ask"
        
        enum Create:String {
            
            case name       = "create_meeting"
            case exit       = "create_meeting_exit"
            case startTime  = "create_meeting_start_time"
            case endTime    = "create_meeting_end_time"
            case location   = "create_meeting_location"
            case done       = "create_meeting_done"
        }
    }    
    
    enum Notification:String {
        
        case join       = "background_notification_join"
        case cancel       = "background_notification_cancel"
        case reschedule  = "background_notification_reschedule"
    }
    
    enum Search:String {
        
        case people  = "search_people"
        case topic   = "search_topic"
        case youtube = "search_youtube"
    }
    
    enum CustomURlScheme:String {
        
        case legacyOfGood = "legacy_of_good"
        case helpCustomer = "help_customer"
    }
}

extension Error {
    
    var code:Int {
        return (self as NSError).code
    }
    
    var domain:String {
        return (self as NSError).domain
    }
}

protocol DAServiceContextDelegate:NSObjectProtocol {
    
    
    func service(_ request:URLRequest?, willSpeakRangeOfSpeechString utterance: String?)
    func service(_ request:URLRequest?, didAuthorizationFail error: Error?)
    
}

protocol ArticulationDelegate:NSObjectProtocol {
    
    func dellAssistant(_ state:ArticulationState, didAuthorize status: SFSpeechRecognizerAuthorizationStatus, error:Error?)
    func dellAssistant(_ state:ArticulationState, didReceive  utterance: String?)
    func dellAssistant(_ state:ArticulationState, didFinish  utterance: String?)
    func dellAssistant(_ state:ArticulationState, willRender  data:Data?)
    func dellAssistant(_ state:ArticulationState, didFail  error:Error?)

}

protocol TranscriptionDelegate:NSObjectProtocol {
    
    func transcription(_ speechRecognizer: SFSpeechRecognizer?, didStart utterance: String?)
    func transcription(_ speechRecognizer: SFSpeechRecognizer?, didReceive utterance: String?)
    func transcription(_ speechRecognizer: SFSpeechRecognizer?, didFinish utterance: String?)
    func transcription(_ speechRecognizer: SFSpeechRecognizer?, didCancel utterance: String?)
    func transcription(_ speechRecognizer: SFSpeechRecognizer?, didError error: Error?)

    func transcription(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool)
}

protocol SpeechSynthesizerDelegate:NSObjectProtocol {
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didStart utterance: AVSpeechUtterance)
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didPause utterance: AVSpeechUtterance)
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didContinue utterance: AVSpeechUtterance)
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance)
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didCancel utterance: AVSpeechUtterance)
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance)
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, progress:Float, attributedText:NSMutableAttributedString)
}

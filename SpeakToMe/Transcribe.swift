//
//  SpeechRecognizer.swift
//  SpeakToMe
//
//  Created by Bogaraj, Jegadesh on 26/03/19.
//  Copyright © 2019 Henry Mason. All rights reserved.
//

import UIKit
import Speech
import CallKit
import Accelerate

class Transcribe:NSObject {

    public  var speechRecognizer: SFSpeechRecognizer? // = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))!
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    private var sentence:String?
    
    var delegate:TranscriptionDelegate?

    var isOnPhoneCall:Bool {
        
        get {
            
            for call in CXCallObserver().calls {
                
                if call.hasEnded == false {
                    return true
                }
            }
            return false
        }
    }
    
    var isRunning:Bool {
        return audioEngine.isRunning
    }
        
    private var timer: Timer?
    
    public var speechTimeoutInterval: TimeInterval = 2 {
        didSet {
            startTimerCounter()
        }
    }
    
    private let taggerOptions: NSLinguisticTagger.Options = [.joinNames, .omitWhitespace]
    private let LEVEL_LOWPASS_TRIG:Float32 = 0.01
    private let averagePowerForChannel0:Float32 = 0.0
    
    private lazy var linguisticTagger: NSLinguisticTagger = {
        let tagSchemes = NSLinguisticTagger.availableTagSchemes(forLanguage: "en")
        return NSLinguisticTagger(tagSchemes: tagSchemes, options: Int(self.taggerOptions.rawValue))
    }()
    
    
    override init() {
        super.init()
        
        speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))!
    }
    
    convenience init(interval:TimeInterval = 2) {
        self.init()
        
        self.speechTimeoutInterval = interval
    }
    
    
    public func startRecording() {
        
        if !isOnPhoneCall {
            
            do {
                
                let audioSession = AVAudioSession.sharedInstance()
                
                try audioSession.setCategory(AVAudioSessionCategoryRecord)
                try audioSession.setMode(AVAudioSessionModeMeasurement)
                try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
                
                
                // Cancel the previous task if it's running.
                if let recognitionTask = recognitionTask {
                    recognitionTask.cancel()
                    self.recognitionTask = nil
                }
                
                if self.audioEngine.isRunning
                {
                    // Remove tap on bus when stopping recording.
                    self.audioEngine.reset()
                    self.audioEngine.inputNode.removeTap(onBus: 0)
                    self.audioEngine.stop()
                }
                
                recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
                
                guard let speechRecognizer = speechRecognizer,
                    let recognitionRequest = recognitionRequest
                    else {
                        return
                        
                }
                
                if speechRecognizer.isAvailable {
                    
                    // Configure request so that results are returned before audio recording is finished
                    recognitionRequest.shouldReportPartialResults = true
                    recognitionRequest.taskHint = .search;
                    // A recognition task represents a speech recognition session.
                    // We keep a reference to the task so that it can be cancelled.
                    recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest) { result, error in
                        
                        if result != nil
                        {
                            self.startTimerCounter()
                            
                            let isFinal = result?.isFinal ?? false

                            if let sentence = result?.bestTranscription.formattedString
                            {
                                /*
                                 self.linguisticTagger.string = sentence
                                 self.textView.text = sentence
                                 self.linguisticTagger.enumerateTags(in: NSMakeRange(0, (sentence as NSString).length), scheme: NSLinguisticTagSchemeNameTypeOrLexicalClass, options: self.taggerOptions) { (tag, tokenRange, _, _) in
                                 let token = (sentence as NSString).substring(with: tokenRange)
                                 //print("\(token) -> \(tag)")
                                 }*/
                                
                                self.sentence = sentence
                                
                                self.delegate?.transcription(speechRecognizer, didReceive: sentence)
                                
                                if isFinal {
                                    
                                    print("Completed..")
                                }
                                
                            }
                        }
                        else  if error != nil {
                            
                            self.delegate?.transcription(speechRecognizer, didError: error)
                            //self.stopRecording()
                        }
                    }
                    
                    let recordingFormat = audioEngine.inputNode.outputFormat(forBus: 0)
                    audioEngine.inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
                        self.recognitionRequest?.append(buffer)
                        
                        /*
                        //https://stackoverflow.com/questions/37821826/continuous-speech-recogn-with-sfspeechrecognizer-ios10-beta
                        
                        //Code to animate a waveform with the microphone volume, ignore if you don't need it:
                        let inNumberFrames:UInt32 = buffer.frameLength;
                        
                        guard let floatChannelData = buffer.floatChannelData
                            else {
                                return
                        }
                        
                        let samples:Float32 = floatChannelData[0][0];
                        //https://github.com/apple/swift-evolution/blob/master/proposals/0107-unsaferawpointer.md
                        var avgValue:Float32 = 0;
                        vDSP_maxmgv(floatChannelData[0], 1, &avgValue, vDSP_Length(inNumberFrames)); //Accelerate Framework
                        //vDSP_maxmgv returns peak values
                        //vDSP_meamgv returns mean magnitude of a vector
                        
                        let avg3:Float32=((avgValue == 0) ? (0-100) : 20.0)
                        let averagePower=(self.LEVEL_LOWPASS_TRIG*avg3*log10f(avgValue)) + ((1-self.LEVEL_LOWPASS_TRIG)*self.averagePowerForChannel0) ;
                        print("AVG. POWER: "+averagePower.description)
                        
                        DispatchQueue.main.async {
                            
                            let fAvgPwr=CGFloat(averagePower)
                            print("AvgPwr: "+fAvgPwr.description)
                            
                            var waveformFriendlyValue=0.5+fAvgPwr //-0.5 is AvgPwrValue when user is silent
                            if(waveformFriendlyValue<0){waveformFriendlyValue=0} //round values <0 to 0
                            //self.waveview.hidden=false
                            //self.waveview.updateWithLevel(waveformFriendlyValue)
                        }
                        */
                    }
                    
                    audioEngine.prepare()
                    
                    try audioEngine.start()
                    subscriber()

                    self.delegate?.transcription(speechRecognizer, didStart: "(Go ahead, I'm listening)")
                }
                else {
                    
                    self.delegate?.transcription(speechRecognizer, availabilityDidChange: speechRecognizer.isAvailable)

                    print("Speech recognition not currently available")
                }
                
            } catch  {
                
                self.delegate?.transcription(speechRecognizer, didError: error)
                print("audioSession properties weren't set because of an error.")
            }
        }
    }
    
    public func stopRecording() {
        
        timer?.invalidate()
        timer = nil
        
        if self.audioEngine.isRunning {
            
            // Remove tap on bus when stopping recording.
            self.audioEngine.inputNode.removeTap(onBus: 0)
            self.audioEngine.stop()
        }
        
        self.recognitionRequest?.endAudio()
        self.recognitionTask?.cancel()

        self.recognitionRequest = nil
        self.recognitionTask = nil
        unsubscriber()
    }
    
    private func startTimerCounter() {
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval:speechTimeoutInterval, target: self, selector: #selector(stopTimerCounter), userInfo: nil, repeats: false)
    }
    
    @objc
    private func stopTimerCounter() {
        
        stopRecording()
        
        self.delegate?.transcription(speechRecognizer, didFinish: sentence)
    }
    
    
    private func subscriber()  {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleInterruption),
                                               name: .AVAudioSessionInterruption,
                                               object: nil)
        
    }
    
    private func unsubscriber()  {
        NotificationCenter.default.removeObserver(self,
                                                  name: .AVAudioSessionInterruption,
                                                  object: nil)
    }
    
    
    @objc func handleInterruption(notification: Notification) {
        
        guard let userInfo = notification.userInfo,
            let typeInt = userInfo[AVAudioSessionInterruptionTypeKey] as? UInt,
            let type = AVAudioSessionInterruptionType(rawValue: typeInt)
            else {
                return
        }
        
        switch type {
        case .began:
            
            self.startRecording()
            
        case .ended:
            if let optionInt = userInfo[AVAudioSessionInterruptionOptionKey] as? UInt {
                let options = AVAudioSessionInterruptionOptions(rawValue: optionInt)
                if options.contains(.shouldResume) {
                    
                    self.stopRecording()
                }
            }
        }
    }
    
    // MARK: SFSpeechRecognizerDelegate
    
    public func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        
        self.delegate?.transcription(speechRecognizer, availabilityDidChange: available)
    }
}

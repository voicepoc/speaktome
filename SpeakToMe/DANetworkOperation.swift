//
//  NetworkOperation.swift
//  SwiftCoding
//
//  Created by Bogaraj, Jegadesh on 4/25/17.
//  Copyright © 2017 Dell. All rights reserved.
//

import Foundation

let CONTENT_TYPE = "application/json;charset=utf-8"
let ACCEPT_TYPE  = "application/json"


//let headers: [String : String] = ["Accept": ACCEPT_TYPE, "Content-Type": CONTENT_TYPE]

class AsyncOperation: Operation {

    override var isAsynchronous: Bool { return true }
    override var isExecuting: Bool { return state == .executing }
    override var isFinished: Bool { return state == .finished }
    
    var state = State.ready {
        willSet {
            willChangeValue(forKey: state.keyPath)
            willChangeValue(forKey: newValue.keyPath)
        }
        didSet {
            didChangeValue(forKey: state.keyPath)
            didChangeValue(forKey: oldValue.keyPath)
        }
    }
    
    enum State: String {
        case ready = "Ready"
        case executing = "Executing"
        case finished = "Finished"
        fileprivate var keyPath: String { return "is" + self.rawValue }
    }
    
    
    /*
     private var _executing: Bool = false
     override var isExecuting: Bool {
     get {
     return _executing
     }
     set {
     if _executing != newValue {
     willChangeValue(forKey: "isExecuting")
     _executing = newValue
     didChangeValue(forKey: "isExecuting")
     }
     }
     }
     
     private var _finished: Bool = false;
     override var isFinished: Bool {
     get {
     return _finished
     }
     set {
     if _finished != newValue {
     willChangeValue(forKey: "isFinished")
     _finished = newValue
     didChangeValue(forKey: "isFinished")
     }
     }
     }
     
     private var _ready: Bool = false;
     override var isReady: Bool {
     get {
     return _ready
     }
     set {
     if _ready != newValue {
     willChangeValue(forKey: "isReady")
     _ready = newValue
     didChangeValue(forKey: "isReady")
     }
     }
     }
     
     override func start() {
     if self.isCancelled {
     _finished = true
     
     } else {
     _ready = true
     main()
     }
     }
     
     override func main() {
     
     if self.isCancelled {
     _finished = true
     
     } else {
     _executing = true
     }
     }
     
     
     public func finish() -> Void {
     _finished  = true
     _executing = false
     }
     */

    override func start() {
        if self.isCancelled {
            state = .finished
        } else {
            state = .ready
            main()
        }
    }
    
    override func main() {
        if self.isCancelled {
            state = .finished
        } else {
            state = .executing
        }
    }
    
    @objc public func finish() -> Void {
        state = .finished
    }
}

class DANetworkOperation: AsyncOperation, URLSessionDelegate {
    
    typealias CompletionBlock = (Data?, Error?) -> Void

    var request:URLRequest?
    var block:CompletionBlock?
    
    override init() {
        super.init()
    }
    
    convenience init(_ request:URLRequest?, completion block:CompletionBlock?) {
        
        self.init()
        self.request = request
        self.block = block
    }
    
    override func start() {
        super.start()
    }
    
    
    override func main() {
        
        super.main()
        
        guard let request = self.request
            else {
                block?(nil, nil)
                self.finish()
                return

        }
        
        let sessionConfiguration = URLSessionConfiguration.default
        let session = URLSession.init(configuration: sessionConfiguration, delegate: self, delegateQueue: nil)
        
        let data = session.dataTask(with: request) { (data, urlResponse, error) in
            
            if self.isCancelled {
                self.finish()
            }
            else
            {
                var _error = error
                
                if _error ==  nil, let statusCode = (urlResponse as? HTTPURLResponse)?.statusCode
                {
                    switch statusCode {
                        
                    case 200, 201:
                        break
                        
                    default:
                        _error =  NSError(domain:"", code:statusCode, userInfo:["userInfo":data ?? ""])
                    }
                    
                }
                
                self.block?(data, _error)
                
                self.finish()
            }
        }
        data.resume()
        
    }
}

//
//  RasaConstants.swift
//  SpeakToMe
//
//  Created by Bogaraj, Jegadesh on 29/03/19.
//  Copyright © 2019 Henry Mason. All rights reserved.
//

import Foundation

struct ENDPOINTS {
    
    static let CONVERSATIONS = "conversations/{sender}/respond"
    static let SAVE_USER = "user/save-user"
}

let HOTWORD = "Hi".lowercased()


struct FALLBACK {
    static let SPEECH_RESPONSE_NULL  = "Sorry, I did not understand you. Can you please provide the infromation again?"
    static let INVALID_RESPONSE_TYPE = "I encountered an error while fetching the requested information. Kindly report this issue in the feedback section of the app preferences."
}

struct RESPONSE {
    static let ATTACHMENT = "attachment"
    static let BUTTONS = "buttons"
}

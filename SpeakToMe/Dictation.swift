//
//  Dictation.swift
//  SpeakToMe
//
//  Created by Bogaraj, Jegadesh on 26/03/19.
//  Copyright © 2019 Henry Mason. All rights reserved.
//

import Foundation
import UIKit
import Speech

final class Dictation:NSObject {

    static let shared = Dictation()
    
    public var speechSynthesizerDelegate:SpeechSynthesizerDelegate?
    
    //MARK:- Speech Synthesizer Variables
    fileprivate var speechSynthesizer: AVSpeechSynthesizer?
    fileprivate var previousSelectedRange: NSRange?
    fileprivate var attributedString = NSMutableAttributedString(string: "")
    
    var totalUtterances: Int    = 0
    var currentUtterance: Int   = 0
    var totalTextLength: Int    = 0
    var spokenTextLengths: Int  = 0
    
    var rate: Float     = 0.0
    var pitch: Float    = 0.0
    var volume: Float   = 0.0
    
    var fontColor:UIColor = UIColor.orange
    
    var preferredVoiceLanguageCode: String?
    

    var text:String = "" {
        didSet{
            attributedString = NSMutableAttributedString(string: text)
        }
    }
    
    //MARK:- Main Funcs
    public override init() {
        super.init()
        
        setup()
    }
    
    // MARK:- AVSpeechSynthesizer
    private func setup() {
        
        speechSynthesizer = AVSpeechSynthesizer()
        speechSynthesizer?.delegate = self
        
        setupSpeechUtteranceRate()
        initializeFontAttribute()
    }
    
    private func setupSpeechUtteranceRate() {
        
        let userDefaults = UserDefaults.standard as UserDefaults
        
        if let theRate: Float = userDefaults.value(forKey: "rate") as? Float {
            rate    = theRate
            pitch   = userDefaults.value(forKey:"pitch") as! Float
            volume  = userDefaults.value(forKey:"volume") as! Float
        }
        else
        {
            rate = AVSpeechUtteranceDefaultSpeechRate
            pitch = 1.0
            volume = 1.0
            
            userDefaults.register(defaults: ["rate": rate, "pitch": pitch, "volume": volume])
        }
    }
    
    private func initializeFontAttribute() {
        
        let range = NSMakeRange(0, text.utf16.count)
        attributedString = NSMutableAttributedString(string: text)
        
        let font = UIFont(name: "Arial", size: 18.0)
        // 3
        attributedString.addAttributes([.font: font ?? UIFont.systemFontSize], range: range)
    }
    
    func speak() {
        
        
        guard let speechSynthesizer = speechSynthesizer
            else {
            return
        }
        
        if !speechSynthesizer.isSpeaking {
            
            /**
             let speechUtterance = AVSpeechUtterance(string: tvEditor.text)
             speechUtterance.rate = rate
             speechUtterance.pitchMultiplier = pitch
             speechUtterance.volume = volume
             speechSynthesizer.speakUtterance(speechUtterance)
             */
            
            let paragraph = text.components(separatedBy: "\n")
            
            totalUtterances   = paragraph.count
            currentUtterance  = 0
            totalTextLength   = 0
            spokenTextLengths = 0
            
            for text in paragraph {
                
                let utterance = AVSpeechUtterance(string: text)
                utterance.rate = rate
                utterance.pitchMultiplier = pitch
                utterance.volume = volume
                utterance.postUtteranceDelay = 0.005
                
                if let voiceLanguageCode = preferredVoiceLanguageCode {
                    let voice = AVSpeechSynthesisVoice(language: voiceLanguageCode)
                    utterance.voice = voice
                }
                
                totalTextLength = totalTextLength + text.utf16.count
                
                speechSynthesizer.speak(utterance)
            }
        }
        else {
            
            speechSynthesizer.continueSpeaking()
        }
    }
    
    
    func pause() {
        speechSynthesizer?.pauseSpeaking(at: AVSpeechBoundary.word)
    }
    
    
    func stop() {
        speechSynthesizer?.stopSpeaking(at: AVSpeechBoundary.immediate)
    }
}


extension Dictation:AVSpeechSynthesizerDelegate {
 
    // MARK: AVSpeechSynthesizerDelegate method implementation
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        spokenTextLengths = spokenTextLengths + utterance.speechString.utf16.count + 1
        
        let progress: Float = Float(spokenTextLengths * 100 / totalTextLength)
        
        speechSynthesizerDelegate?.speechSynthesizer(synthesizer, progress: progress/100, attributedText: attributedString)
        
        if currentUtterance == totalUtterances {
            //unselectLastWord()
            previousSelectedRange = nil
        }
        speechSynthesizerDelegate?.speechSynthesizer(synthesizer, didFinish: utterance)
    }
    
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didStart utterance: AVSpeechUtterance) {
        currentUtterance = currentUtterance + 1
        speechSynthesizerDelegate?.speechSynthesizer(synthesizer, didStart: utterance)
    }
    
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance) {
        
        /*
        let progress: Float = Float(spokenTextLengths + characterRange.location) * 100 / Float(totalTextLength)
        
        speechSynthesizerDelegate?.speechSynthesizer(synthesizer, progress: progress/100, attributedText: self.attributedString)

        // Determine the current range in the whole text (all utterances), not just the current one.
        let rangeInTotalText = NSMakeRange(spokenTextLengths + characterRange.location, characterRange.length)
        
        // Select the specified range in the textfield.
        
        // Store temporarily the current font attribute of the selected text.
        let currentAttributes = self.attributedString.attributes(at: rangeInTotalText.location, effectiveRange: nil)
        let fontAttribute = currentAttributes[NSAttributedString.Key.font] ?? UIFont.boldSystemFont(ofSize: 17)
        
        // Assign the selected text to a mutable attributed string.
        let attributedSubString = NSMutableAttributedString(string: self.attributedString.attributedSubstring(from: rangeInTotalText).string)
        
        // Make the text of the selected area orange by specifying a new attribute.
        //attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: fontColor, range: NSMakeRange(0, attributedString.length))
        
        // Make sure that the text will keep the original font by setting it as an attribute.
        attributedSubString.addAttribute(NSAttributedString.Key.font, value: fontAttribute, range: NSMakeRange(0, attributedSubString.string.utf16.count))
        
        attributedSubString.replaceCharacters(in: rangeInTotalText, with: attributedSubString)
        
        // If there was another highlighted word previously (orange text color), then do exactly the same things as above and change the foreground color to black.
        if let previousRange = previousSelectedRange {
            let previousAttributedText = NSMutableAttributedString(string: attributedSubString.attributedSubstring(from: previousRange).string)
            //previousAttributedText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSMakeRange(0, previousAttributedText.length))
            //previousAttributedText.addAttribute(NSAttributedString.Key.font, value: fontAttribute, range: NSMakeRange(0, previousAttributedText.length))
            
            attributedSubString.replaceCharacters(in: previousRange, with: previousAttributedText)
        }
        
        // Keep the currently selected range so as to remove the orange text color next.
        previousSelectedRange = rangeInTotalText*/
    }
    
}


extension Dictation {
    
    fileprivate func unselectLastWord() {
        
        if let selectedRange = previousSelectedRange {
            
            let currentAttributes = attributedString.attributes(at: selectedRange.location, effectiveRange: nil)
            
            let fontAttribute = currentAttributes[NSAttributedString.Key.font] ?? UIFont.boldSystemFont(ofSize: 17)
            
            
            let attributedWord = NSMutableAttributedString(string: attributedString.attributedSubstring(from: selectedRange).string)
            
            
            attributedWord.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSMakeRange(0, attributedWord.length))
            attributedWord.addAttribute(.font, value: fontAttribute, range: NSMakeRange(0, attributedWord.length))
            
            // Update the text storage property and replace the last selected word with the new attributed string.
            
            attributedString.replaceCharacters(in: selectedRange, with: attributedWord)
        }
    }
}

//
//  DAResponse.swift
//  DellAssistant
//
//  Created by Bogaraj, Jegadesh on 02/04/19.
//  Copyright © 2019 Henry Mason. All rights reserved.
//

import Foundation


typealias DAResponse = [DADocument]
typealias DAFormResponse = [DAForm]

public struct DAElement:Codable {
    
    var intent:String
    var speech:[String]?

    enum CodingKeys: String, CodingKey {
        case intent
        case speech
    }

}


public struct DADocument:Codable {
    
    var response:DAElement
    var recipientID: String
    
    enum CodingKeys: String, CodingKey {

        case response = "attachment"
        case recipientID = "recipient_id"
    }
}

public struct DAForm:Decodable {
    
    var response:DAElement
    var buttons:[DAButton]
    var recipientID: String
    
    enum CodingKeys: String, CodingKey {
        
        case recipientID = "recipient_id"
        case buttons = "buttons"
        case intent = "payload"
        case speech = "text"
    }
    
    public init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        let speech = try values.decode(String.self, forKey: .speech)
        
        buttons     = try values.decode([DAButton].self, forKey: .buttons)
        recipientID = try values.decode(String.self, forKey: .recipientID)
        
        response    = DAElement(intent: buttons.first?.intent ?? "", speech: [speech])
    }
}

public struct DAButton:Decodable {
    
    var intent:String
    
    enum CodingKeys: String, CodingKey {
        
        case intent = "payload"
    }
}

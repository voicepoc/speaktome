//
//  DAUser.swift
//  DellAssistant
//
//  Created by Bogaraj, Jegadesh on 03/04/19.
//  Copyright © 2019 Henry Mason. All rights reserved.
//

import Foundation
import UIKit
//["dellUserId":"","fullName":"","userName":"","userTimeZone":"","pushNotificationKey":""]
public class DAUser:Codable {
    
    var userid:Int?
    var name:String?
    var email:String?
    var timezone:String?
    var status:Int?
    var isFromDell:String?
    var recepientid:Int?
    var createdAt:Int?
    var updatedAt:Int?
    var timeZoneCode:String?
    
    enum CodingKeys: String, CodingKey {
        
        case userid      = "dellUserId"
        case name        = "fullName"
        case email       = "userName"
        case timezone    = "timezone"
        case status      = "status"
        case isFromDell  = "isFromDell"
        case recepientid = "id"
        case createdAt   = "createdAt"
        case updatedAt   = "updatedAt"
        case timeZoneCode = "timezoneCode"

    }
}

public struct DAUserResponse:Decodable {
    
    var user:DAUser?
    var status:Status
    
    enum CodingKeys: String, CodingKey {
        
        case user = "result"
        case message = "status_message"
        case code = "status_code"
    }
    
    public init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let message = try values.decode(String.self, forKey: .message)
        let code = try values.decode(Int.self, forKey: .code)
        
        status = Status(message: message, code: code)
        user = try values.decode(DAUser.self, forKey: .user)
    }
}

//https://hackernoon.com/everything-about-codable-in-swift-4-97d0e18a2999

public struct Status:Codable {
    
    var message:String
    var code:Int
    
    enum CodingKeys: String, CodingKey {
        
        case message = "status_message"
        case code = "status_code"
    }
}

